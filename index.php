<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>STRING FUNCTIONS </title>
    </head>
    <body>
        <?php
    $str1= "I live in ";
    $str2= "Bangladesh";
    $str3=$str1.$str2;
    echo $str3 ."<br>";//I live in Bangladesh
    $str3=$str1. " ";
    $str3=$str3.$str2;
      
   echo $str3 . "<br>";//I live in Bangladesh
   
   $str3=$str1."";
   
   $str3.=$str2; // dot net equal means $str3+$str2=$str3
   echo $str3; //I live in Bangladesh
       
   //1. converting a string a to uppercase
   //2. Converting a string to lowercase
   //3. Converting first letter of a string to uppercase
   //4. Converting first letter of each word a string a uppercase
   //3. Converting first letter of a string to lowercase
        ?>
        <br>
        
        <b>1. Uppercase of $str3 is:</b> <br><?php echo strtoupper($str3); //I LIVE IN BANGLADESH ?> <br><br>
        <b>2. Lowsercase of $str3 is:</b> <br> <?php echo strtolower($str3); //i live in bangladesh ?> <br><br>
        <b>3. Converting first letter of a string to uppercase of $str3 is:</b> <br> <?php echo ucfirst($str3); //I live in Bangladesh ?> <br><br>
        <b>4. Converting first letter of each word uppercase of $str3 is:</b> <br> <?php echo ucwords($str3); //I Live In Bangladesh ?> <br><br>
        <b>5. Converting first letter of a string to lowercase of $str3 is:</b> <br> <?php echo lcfirst($str3); //i live in Bangladesh ?> <br><br>
        <?php
        //6. Counting the lenght of a string.
        echo "<b> 6. The lenght of \$str3 is : </b>" ."<br>";
        echo strlen($str3);//20
        
        echo "<br> <br>";
        // 7. Removing the extra white space into a string
        $coder1="I love u";
        $coder2=" Shaila";
        $coder2=  trim($coder2);
        $coder3=$coder1.$coder2;
        echo "<b>7. Removing the extra white space into a string</b>" ."<br>";
        echo $coder3. "<br> <br>";//I love uShaila 
        
        // Finding one string to another string.
        echo "<b>8. Finding one string to another string </b>" ."<br>";
        echo strstr($coder1, "love")."<br> <br>";
        
         //Replacing string
        echo "<b>9. Replacing string to another string </b>" ."<br>";
        echo str_replace("u", "Ashru", $coder1)."<br><br>";
        
        // Substring into a string
        echo "<b>10. Substring into a string </b>" ."<br>";
        echo substr($coder1,3,3); //ove 
        echo "<br> <br>";
        
        //Getting position of a string into another string
        echo "<b>11. Getting position of a string into another string </b>" ."<br>";
        echo strpos($coder1, "love");//2 
       
        echo "<br> <br>";
        $a= 10;
        $b= 30;
        $c= 200;
        $z= $a+$b+$c;
        
        echo "<b> The value of z is : </b>". "<br>" ;
        echo $z; //240
        echo "<br>";
        
        $x= ((20+30*9)-(20+20))/(20+30);
        echo $x; // 2
        
         echo "<br>";
         $coder= 4;
         $coder = $coder + 4;
         
         echo $coder; //8
         
         echo "<br>";
         
         $coder += 4;
         echo $coder; //12 (+= means $coder add with 4 and output is 12)
         
          echo "<br>";
          
         $coder -= 3;
         echo $coder; //9 (-= means $coder substrck with 3 and output is 9)
         
         echo "<br>";
         
         $coder *= 3;
         echo $coder; //27 (-= means $coder multiplay with 3 and output is 27)
         
          echo "<br>";
         
         $coder /= 9;
         echo $coder; //3 (-= means $coder divide with 3 and output is 3)
         
         echo "<br>";
         
       
                 
         $mazed =20;
         $mazed = $mazed++;
         
         echo $mazed; //20 (1 add in next line)
         
        echo "<br>";
        
        $mazed =++$mazed;
        echo $mazed; //21 (1 add in current line)
        
        
        echo "<br>";
        
        $a1=4;
        $a2=3;
        $a3=$a1/$a2;
        echo $a3; //1.333333333333
        
        echo "<br>";
        
        echo "Round: " .  round($a3, 2); //1.33 (round after 2 numbers)
        echo "<br>";
        echo "Ceil: " . ceil($a3); //2 (if found any number of dosomic then ceil add 1)
        echo "<br>";
        
        echo "Floor: " . floor($a3); //1 (floor function ignore of dosomic number)
         
        echo "<br>";
        echo "Square root: " . sqrt($a3); //1.1547005383793 
        echo "<br>";
        
        echo "Power: " . pow($a3 , 2); // 1.7777777777778
        echo "<br>";
        echo "Power: " . pow(8 , 2); // 64
        
        echo "<br>";
        echo "Random Number: " .  rand(); //14828 (Number are frequently changed) // maximun rand number 32767
        echo "<br>";
        echo "Random Number: " .  rand(10,100); // 16 (Number are frequently changed only number 10-100)
        echo "<br>";
        echo "Alternate process of Random Number: " .  mt_rand(); //2131832986  (use for hauge number)// maximun rand number 2^32
        echo "<br>";
        echo "Alternate process of Random Number: " .  mt_rand(10,100); //61(Number are frequently changed only number 10-100)
        
        
        ?>
        
        
      
        
        
        
    </body>
</html>
